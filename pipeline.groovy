pipeline {
    agent any

    environment {
		registry = "lecioneto/zzz"
        registryCredential = "dockerhub_id" 
        dockerImage = ''
    }

    stages {
    	stage('Clone Repository') {
    		steps {  
                git branch: "main", url: 'https://gitlab.com/lecioneto/zzz.git'
			}
    	}
    	stage('Build Docker Image') {
            steps{
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
            }
        }
    	stage('Send image to Docker Hub') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential) {
                        dockerImage.push()
                    }
                }
            }
        }
    	stage('Cleaning up') {
        	steps {
            	sh "docker rmi $registry:$BUILD_NUMBER"
        	}
		}
        stage('Update WWW') {
        	steps {
            	sh "ssh ubuntu@172.31.17.95 docker run -p 80:80 --rm $registry:$BUILD_NUMBER"
        	}
		}
    }
}